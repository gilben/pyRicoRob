# Copyright (c) Nicholas Gilbert 2018
# Licensed under the MIT license, see LICENSE in the root repository

import curses
import logging
from board import *
from curses import wrapper

logging.basicConfig(filename='ricorob.log', level=logging.DEBUG)
logging.info("Begin logging")


def refreshall(stdscr, box):
    stdscr.refresh()
    box.refresh()

def updateboard(window, gameboard):
    "Updates the board box to match the contents of the gameboard"
    for i in range(0, gameboard.width):
        for j in range(0, gameboard.height):
            if i % 2 == 0 and j % 2 == 0:
                window.addch(j + 1, i + 1, "0")
                b = gameboard.grab_bot(j, i)
                if b != None: #if it's a bot
                    window.addch(j + 1, i + 1, b.char)
            else:
                ws = gameboard.is_wall((i, j))
                if ws != None:
                    window.addch(j + 1, i + 1, ws)
            #y += 2

        #x += 2
        #y = 0

def main(stdscr):
    stdscr.clear()
    stdscr.border(0)

    box = curses.newwin(34, 34, 0, 0)
    box.box()

    refreshall(stdscr, box)

    gameboard = board()
    updateboard(box, gameboard)

    refreshall(stdscr, box)
    stdscr.getkey()
    
    updateboard(box, gameboard)

    refreshall(stdscr, box)
    stdscr.getkey()

wrapper(main)