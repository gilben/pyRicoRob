# Copyright (c) Nicholas Gilbert 2018
# Licensed under the MIT license, see LICENSE in the root repository

class bot:
    x = 0
    y = 0
    color = ""
    char = ''
    def __init__(self, color, x, y):
        self.x = x
        self.y = y
        self.color = color
        self.char = self.color[0]

class board:
    width = 32
    height = 32
    bots = []
    walls = {}
    def __init__(self):
        self.width = 32
        self.height = 32
        #self.board = [["0" for i in range(self.width)] for j in range(self.height)]
        self.bots = [bot("red", 1, 1), bot("blue", 2, 2), bot("green", 3, 3), bot("yellow", 4, 4)]
        self.walls = {
            (7,0): "|",
            (21, 0): "|",
            (29, 2): "|",
            (28, 3): "-"
        }
    def grab_bot(self, x, y):
        "Returns a bot from a given x and y coordinates"
        for bot in self.bots:
            if bot.x == x and bot.y == y:
                return bot
        return None

    def is_wall(self, xy):
        if xy in self.walls:
            return self.walls[xy]
        else:
            return None
           


"""
    def is_wall(self, x, y, dir=None):
        found = None
        if (x, y) in self.walls:
            found = self.walls[(x, y)]
        else:
            return None

        if dir == None:
            return found

        target = ()
        if dir == "up":
            target = (x, y - 1 )     
        elif dir == "down":
            target = (x, y + 1)
        elif dir == "left":
            target = (x - 1, y)
        else:
            target = (x + 1, y)

        for w in found:
            if w == target:
                return w

        return None
        """